<!--- BEGIN HEADER -->
# Changelog

All notable changes to this project will be documented in this file.
<!--- END HEADER -->

## [0.0.2](https://gitlab.com/paquetes-bittacora/bpanel4-paginas/compare/v0.0.1...v0.0.2) (2021-11-08)

---

# Changelog

All notable changes to `page` will be documented in this file.

## 1.0.0 - 202X-XX-XX

- initial release
