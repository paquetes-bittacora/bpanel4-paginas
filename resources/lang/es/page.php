<?php

return [
    'page_list' => 'Listado de páginas',
    'title' => 'Título',
    'subtitle' => 'Subtítulo',
    'long_text' => 'Texto completo',
    'short_text' => 'Texto resumen',
    'add' => 'Crear página',
    'create_default_language' => '<i class="fas fa-info"></i>&nbsp;&nbsp;Al crear un nuevo contenido, se creará en el idioma principal. Si desea añadir traducciones, guarde primero el contenido en el idioma principal.',
    'created' => 'La página fue creada correctamente.',
    'updated' => 'La página fue actualizada correctamente.',
    'deleted' => 'La página fue borrada correctamente.',
    'not-deleted' => 'No se pudo borrar la página.',
    'active' => 'Publicada',
    'edit' => 'Editar página',
    'index' => 'Listar páginas',
    'create' => 'Crear página',
    'show' => 'Mostrar página',
    'destroy' => 'Eliminar página',
    'store' => 'Añadir página',
    'update' => 'Actualizar página',
    'sidebar' => 'Barra lateral',
    'include_team' => 'Incluir equipo'
];
