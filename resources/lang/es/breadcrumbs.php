<?php

return [
    'index' => 'Listado',
    'page'=> 'Páginas de contenido',
    'create' => 'Nueva página',
    'edit' => 'Editar página'
];
