<td>
    {{$row->title}}
</td>
<td>
    @livewire('language::datatable-languages', ['model' => $row, 'createRouteName' => 'page.edit', 'editRouteName' => 'page.edit'], key('page-language-'.$row->id))
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-page-'.$row->id))
</td>
<td>
    <div class="d-flex justify-content-center align-items-center">
        <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
            <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
            <span class="pos-rel">
                {{$row->order_column}}
            </span>
        </span>
    </div>
</td>
<td>
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'page', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'la página?'], key('page-buttons-'.$row->id))
</td>
