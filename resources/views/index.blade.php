@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Páginas de contenido')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('page::page.page_list') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('page::page-datatable')
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets_bpanel/js/livewire-sortable.js') }}"></script>
@endpush
