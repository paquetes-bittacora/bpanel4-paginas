@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Crear página')

@section('content')
    <p class="alert alert-success">
        {!!  __('page::page.create_default_language') !!}
    </p>
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('page::page.add') }}</span>
            </h4>
            <img src="{{asset('img/flags/'.$language.'.png')}}" alt="{{ $language }}">
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('page.store')}}" enctype="multipart/form-data">
            @csrf
            @livewire('form::input-text', ['name' => 'title', 'labelText' => __('page::page.title'), 'required'=>true])
            @livewire('utils::tinymce-editor', ['name' => 'long_text', 'labelText' => __('page::page.long_text')])
            @livewire('utils::tinymce-editor', ['name' => 'short_text', 'labelText' => __('page::page.short_text')])
            @livewire('form::input-checkbox', ['name' => 'active', 'value' => 1, 'labelText' => __('page::page.active'), 'bpanelForm' => true, 'checked' => true])
            @livewire('form::input-checkbox', ['name' => 'include_team', 'value' => 1, 'labelText' => __('page::page.include_team'), 'bpanelForm' => true, 'checked' => false])
            @livewire('content-multimedia::upload-content-multimedia-widget', ['model' => null, 'allowedFormats' => $allowedFormats, 'allowedExtensions' => $allowedExtensions])
            @livewire('seo::seo-fields')


            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'save'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @livewire('form::input-hidden', ['name' => 'locale', 'value'=> $language])
        </form>
        @livewire('multimedia::multimedia-images-library')
    </div>
@endsection
