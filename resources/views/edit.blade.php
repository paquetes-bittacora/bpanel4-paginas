@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Editar página')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('page::page.edit') }}</span>
            </h4>
            @include('language::partials.form-languages', ['model' => $page, 'edit_route_name' => 'page.edit',  'currentLanguage' => $language])
        </div>

{{--  ###      @livewire('utils::media-library')--}}
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ route('page.update', ['model' => $page, 'locale' => $language]) }}" enctype="multipart/form-data">
            @csrf
            @livewire('form::input-text', ['name' => 'title', 'labelText' => __('page::page.title'), 'required'=>true, 'value' => $page->title])
{{--  ###            @livewire('form::input-text', ['name' => 'subtitle', 'labelText' => __('page::page.subtitle'), 'required'=>false, 'value' => $page->subtitle])--}}
            @livewire('utils::tinymce-editor', ['name' => 'long_text', 'labelText' => __('page::page.long_text'), 'value' => $page->long_text])
            @livewire('utils::tinymce-editor', ['name' => 'short_text', 'labelText' => __('page::page.sidebar'), 'value' => $page->short_text])
            @livewire('form::input-checkbox', [
            'name' => 'active',
            'value' => 1,
            'labelText' => __('page::page.active'),
            'bpanelForm' => true,
            'checked' => $page->active == 1,
            ])

            @livewire('form::input-checkbox', [
            'name' => 'include_team',
            'value' => 1,
            'labelText' => __('page::page.include_team'),
            'bpanelForm' => true,
            'checked' => $page->include_team == 1,
            ])
            @livewire('content-multimedia::upload-content-multimedia-widget', ['model' => $page, 'allowedFormats' => $allowedFormats, 'allowedExtensions' => $allowedExtensions])
            @livewire('content-multimedia-images::content-multimedia-images-widget', ['module' => 'page', 'contentId' => $page->content->id, 'permission' => 'page.edit'])
{{--  ###            @livewire('content-multimedia-documents::content-multimedia-documents-widget', ['contentId' => $page->content->id, 'permission' => 'page.edit'])--}}
{{--  ###            @livewire('content-multimedia-links::content-multimedia-links-widget', ['contentId' => $page->content->id, 'permission' => 'page.edit'])--}}
            @livewire('seo::seo-fields', ['model' => $page])


            @livewire('utils::created-updated-info', ['model' => $page])

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'save'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @livewire('form::input-hidden', ['name' => 'locale', 'value'=> $language])
            @livewire('form::input-hidden', ['name' => 'id', 'value'=> $page->id])
            <input type="hidden" name="_method" value="PUT">
        </form>
        @livewire('multimedia::multimedia-images-library')
    </div>
@endsection
