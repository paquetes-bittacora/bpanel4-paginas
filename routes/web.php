<?php

use Bittacora\Page\Http\Controllers\PageController;
use Bittacora\Page\Http\Controllers\PublicPageController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;

Route::prefix('bpanel')->middleware(['web', 'auth', 'admin-menu'])->name('page.')->group(function(){
    Route::get('page', [PageController::class, 'index'])->name('index');
    Route::get('page/{page}/show', [PageController::class, 'show'])->name('show');
    Route::get('page/create/language/{locale?}', [PageController::class, 'create'])->name('create');
    Route::post('page/store', [PageController::class, 'store'])->name('store');
    Route::get('page/{model}/edit/language/{locale?}', [PageController::class, 'edit'])->name('edit');
    Route::put('page/{model}/update/language/{locale?}', [PageController::class, 'update'])->name('update');
    Route::delete('page/{page}', [PageController::class, 'destroy'])->name('destroy');
    Route::post('page/reorder', [PageController::class, 'reorder'])->name('reorder');
    Route::get('page/multimedia', [PageController::class, 'multimedia'])->name('multimedia');
});

Route::middleware(['web'])->name('publicPage.')->group(function(){
    Route::get(__('general.page-slug').'/{slug}', [PublicPageController::class, 'show'])->name('show');
});
