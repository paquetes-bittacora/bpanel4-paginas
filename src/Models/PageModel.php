<?php

namespace Bittacora\Page\Models;

use Bittacora\Content\Models\ContentModel;
use Illuminate\Database\Eloquent\Model;
use Rinvex\Categories\Traits\Categorizable;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;
use Wildside\Userstamps\Userstamps;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageModel extends Model implements Sortable
{
    use HasTranslations;
    use SortableTrait;
    use Userstamps;
    use HasTranslatableSlug;
    use SoftDeletes;

    public $sortable = [
        'sort_when_creating' => true
    ];

    protected $fillable = [
        'title',
        'subtitle',
        'short_text',
        'long_text',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'active',
        'include_team'
    ];

    public $translatable = [
        'title',
        'subtitle',
        'short_text',
        'long_text',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    protected $with = ['content'];
    protected $table = 'pages';

    public function content()
    {
        return $this->morphOne(ContentModel::class, 'model');
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::createWithLocales([$this->getLocale()])
            ->generateSlugsFrom(function($model, $locale) {
                return "{$model->title}";
            })
            ->saveSlugsTo('slug');
    }
}
