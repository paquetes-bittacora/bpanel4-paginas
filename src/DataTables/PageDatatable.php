<?php

namespace Bittacora\Page\DataTables;

use Bittacora\Page\Models\PageModel;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Options\Plugins\RowReorder;
use Yajra\DataTables\Services\DataTable;
use Bittacora\Utils\DataTable\Reorder;

class PageDatatable extends DataTable
{
    use RowReorder, Reorder;

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('title', function (PageModel $page){
                return $page->title;
            })
            ->addColumn('active', function($query){
                return view ('page::partials.active')->with([
                    'idField' => $query->id,
                    'name' => 'active',
                    'checked' => $query->active,
                ]);
            })
            ->addColumn('action', function ($query) {
                return view('page::partials.actions')->with([
                    'id' => $query->id,
                    'scope' => 'language',
                    'message' => 'el idioma',
                    'disabled' => false,
                ]);
            });
    }

    public function query(PageModel $languageModel)
    {
        return $languageModel->newQuery()->orderBy('order_column');
    }

    public function html()
    {
        return $this->builder()
            ->rowReorder([
                'update' => true, //Actualiza la tabla una vez se ha completado el reorder
                'selector' => 'td:nth-child(6)' //Nº de columna que va a ser la que reordene
            ])
            ->rowId('id') //Necesario para el reorder
            ->addTableClass('d-style w-100 table text-dark-m1 text-95 border-y-1 brc-black-tp11 collapsed')
            ->columns($this->getColumns())
            ->paging(true)
            ->minifiedAjax()
            ->dom("<'row'<'col-4'l><'col-5'f><'col-3'B>><'row'<'col-12't>><'row'<'col-12'p>><'clear'>")
            ->language('/vendor/datatables/spanish.json')
            ->drawCallbackWithLivewire()
            ->buttons(
                Button::make('export')
                    ->text('<i class="fa fa-download"></i> <span class="d-none">Descargar</span>'),
                Button::make('print')
                    ->text('<i class="fa fa-print"></i> <span class="d-none">Imprimir</span>'),
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('title')->title('Título')
                ->addClass('d-flex flex-row align-items-center justify-content-center  ')
                ->orderable(false),
            Column::computed('active')->title('Activo')
                ->printable(false)
                ->exportable(true)
                ->width('100px')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center'),
            Column::computed('order_column')->title('Orden')
                ->searchable(false)
                ->printable(false)
                ->exportable(true)
                ->orderable(true)
                ->width('100px')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center'),
            Column::computed('action')->title('Acciones')
                ->printable(false)
                ->exportable(false)
                ->width('100px')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Idiomas_' . date('YmdHis');
    }
}
