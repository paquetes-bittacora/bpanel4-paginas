<?php

namespace Bittacora\Page;

use Bittacora\Page\Http\Livewire\PageDatatable;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Page\Commands\InstallCommand;

class PageServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('page')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_page_table')
            ->hasCommand(InstallCommand::class);
    }

    public function register(){
        $this->app->bind('page', function($app){
            return new Page();
        });
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ .'/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'page');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'page');
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class
            ]);
        }
        Livewire::component('page-datatable-checkbox', Http\Livewire\DatatableCheckbox::class);
        Livewire::component('page::page-datatable', PageDatatable::class);
    }
}
