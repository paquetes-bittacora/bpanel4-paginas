<?php

namespace Bittacora\Page\Http\Livewire;

use Bittacora\Page\Models\PageModel;
use Livewire\Component;

class DatatableCheckbox extends Component
{
    public $name;
    public $idField;
    public $value = null;
    public $checked = false;
    public $multiple = false;
    public $title = null;
    public $labelText;
    public $page;

    public function mount(PageModel $page){
        $this->page = $page;
    }

    public function render()
    {
        return view('page::livewire.datatable-checkbox')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'checked' => $this->checked,
            'multiple' => $this->multiple,
            'labelText' => $this->labelText,
        ]);
    }

    public function toggle(PageModel $pageModel){
        $pageModel->update(['active' => !$pageModel->active]);
        $this->checked = $pageModel->active;
    }
}
