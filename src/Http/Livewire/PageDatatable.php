<?php

namespace Bittacora\Page\Http\Livewire;

use Bittacora\Content\ContentFacade;
use Bittacora\Page\Models\PageModel;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class PageDatatable extends DataTableComponent
{
    public int $contentId;
    public bool $reordering = true;
    public string $reorderingMethod = 'reorder';
    public bool $showPerPage = true;
    public bool $showPagination = true;
    public bool $showSearch = true;
    public array $perPageAccepted = [10,25,50,100];
    public string $emptyMessage = "El registro de páginas está vacío";


    public function columns(): array
    {
        return [
            Column::make('Título', 'title')->addClass('w-50'),
            Column::make('Idiomas')->addClass('w-10 text-center'),
            Column::make('Activo', 'active')->addClass('w-10 text-center'),
            Column::make('Orden', 'order_column')->addClass('w-10 text-center'),
            Column::make('Acciones')->addClass('w-10')
        ];
    }

    public function query()
    {
        return PageModel::query()->orderBy('order_column', 'ASC')->
        when($this->getFilter('search'), fn ($query, $term) => $query->where('title->es', 'like', '%'.strtoupper($term).'%')
            ->orWhere('title->es', 'like', '%'.strtolower($term).'%')->orWhere('title->es', 'like', '%'.ucfirst($term).'%'));
    }

    public function rowView(): string
    {
        return 'page::livewire.page-datatable';
    }

    public function reorder($list){
        foreach($list as $item){
            PageModel::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }

    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar'
        ];
    }

    public function bulkDelete(){
        if(count($this->selectedKeys())){
            foreach($this->selectedKeys() as $key){
                $page = PageModel::where('id', $key) -> first();
                $page->delete();
                ContentFacade::deleteModelContent($page);
            }
            $ids = PageModel::ordered()->pluck('id');
            PageModel::setNewOrder($ids);
            $this->resetAll();
        }
    }
}
