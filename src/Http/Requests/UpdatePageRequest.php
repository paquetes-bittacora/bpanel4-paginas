<?php

namespace Bittacora\Page\Http\Requests;

use Bittacora\Multimedia\Models\Multimedia;
use Bittacora\Seo\SeoFacade;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePageRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        if($this->has('active')){
            $this->request->add(['active' => 1]);
        }else{
            $this->request->add(['active' => 0]);
        }

        if($this->has('include_team')){
            $this->request->add(['include_team' => 1]);
        }else{
            $this->request->add(['include_team' => 0]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:255',
            'file[]' => 'file|max:'. (config('multimedia.max_file_size')/1024) .'|mimes:'.Multimedia::$allowedExtensions,
        ];

        $rules = SeoFacade::addRequestValidationRules($rules, 'pages', $this->id);

        return $rules;
    }
}
