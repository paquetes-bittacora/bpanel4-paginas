<?php


namespace Bittacora\Page\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Content\ContentFacade;
use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Bittacora\ContentMultimediaLinks\ContentMultimediaLinksFacade;
use Bittacora\Language\DataTables\LanguageDatatable;
use Bittacora\Language\LanguageFacade;
use Bittacora\Multimedia\Models\Multimedia;
use Bittacora\Multimedia\MultimediaFacade;
use Bittacora\Page\Http\Requests\StorePageRequest;
use Bittacora\Page\Http\Requests\UpdatePageRequest;
use Bittacora\Page\Models\PageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class PageController extends Controller
{
    public function create($language = null)
    {
        $this->authorize('page.create');

        if (empty($language)) {
            $defaultLanguage = LanguageFacade::getDefault();
            $language = $defaultLanguage->locale;
        }

        return view('page::create', ['language' => $language, 'allowedFormats' => Multimedia::$allowedFormats, 'allowedExtensions' => Multimedia::$allowedExtensions]);
    }

    public function edit(PageModel $model, LanguageDatatable $languageDatatable, $language = null)
    {
        if (empty($language)) {
            $language = LanguageFacade::getDefault()->locale;
        }

        $allowedFormats = Multimedia::$allowedFormats;
        $allowedExtensions = Multimedia::$allowedExtensions;
        $model->setLocale($language);
        return view('page::edit', ['language' => $language, 'page' => $model, 'multimedia' => MultimediaFacade::getAll(),
            'allowedFormats' => $allowedFormats, 'allowedExtensions' => $allowedExtensions, 'languageDatatable' => $languageDatatable]);
    }

    public function index()
    {
        $this->authorize('page.index');
        return view('page::index');
    }

    /**
     * @param Request $request
     */
    public function reorder(Request $request)
    {
        $collect = collect($request->json()->all());
        PageModel::setNewOrder($collect->pluck('id'), $collect->first()['position']);
    }

    public function show(PageModel $page)
    {
        $locale = LanguageFacade::getDefault()->locale;
        return redirect(route('page-public.show', [
            'locale' => $locale,
            'slug' => $page->getTranslation('slug', $locale)
        ]));
    }

    public function showPublic($slug)
    {
        $locale = app()->getLocale();
        $model = PageModel::where('slug->'.$locale, $slug)->where('active', 1)->with('content')->firstOrFail();
        dd($model);
        return view('page.page', compact('model'));
    }

    public function store(StorePageRequest $request)
    {
        $this->authorize('page.store');

        $page = new PageModel();
        $page->setLocale($request->input('locale'));

        $page->fill($request->all());
        $page->save();

        ContentFacade::associateWithModel($page);
        if(!empty($_FILES['file']['name'][0])){
            ContentFacade::associateWithMultimedia($page, $_FILES['file']);
        }

        return redirect()->route('page.index')->with(['alert-success' => __('page::page.created')]);
    }

    public function update(UpdatePageRequest $request, PageModel $model, $locale)
    {
        $this->authorize('page.update');
        $model->setLocale($request->input('locale'));

        $model->fill($request->all());
        $model->save();

        ContentFacade::associateWithModel($model);
        if(!empty($_FILES['file']['name'][0])){
            ContentFacade::associateWithMultimedia($model, $_FILES['file']);
        }

        return redirect()->route('page.index')->with(['alert-success' => __('page::page.updated')]);
    }

    public function destroy(PageModel $page)
    {
        $this->authorize('page.destroy');

        if ($page->delete()) {
            // Reordeno después de borrar
            $ids = PageModel::ordered()->pluck('id');
            PageModel::setNewOrder($ids);

            // Borro el content
            ContentFacade::deleteModelContent($page);

            Session::put('alert-success', __('page::page.deleted'));
        } else {
            Session::put('alert-warning', __('page::page.not-deleted'));
        }
        return redirect(route('page.index'));
    }
}
