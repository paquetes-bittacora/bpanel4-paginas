<?php

namespace Bittacora\Page\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Bittacora\Page\Models\PageModel;
use Illuminate\Http\Request;

class PublicPageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = PageModel::where('slug->'.app()->getLocale(), $slug)->where('active', 1)->first();
        if(empty($page)){
            return abort(404);
        }
        $page->images = ContentMultimediaFacade::retrieveContentImages('page', $page->content->id);

        return view('page', compact('page'));
    }

}
