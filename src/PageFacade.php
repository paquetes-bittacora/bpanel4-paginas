<?php

namespace Bittacora\Page;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Page\Page
 */
class PageFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'page';
    }
}
