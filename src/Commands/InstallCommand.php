<?php

namespace Bittacora\Page\Commands;

use Bittacora\AdminMenu\AdminMenuFacade;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InstallCommand extends Command
{
    public $signature = 'page:install';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('Registrando elementos del menú...');
        $module = AdminMenuFacade::createModule('contents', 'page', 'Páginas', 'fa fa-file');
        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');
        $this->comment('Hecho');

        $this->comment('Dando permisos al administrador...');
        $permissions = ['index', 'create', 'show', 'edit', 'destroy', 'store', 'update'];
        $adminRole = Role::findOrCreate('admin');
        foreach($permissions as $permission){
            $permission = Permission::firstOrCreate(['name' => 'page.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }

        $this->comment('Hecho');
    }
}
