<?php

use Bittacora\Seo\SeoFacade;
use Bittacora\Tabs\Tabs;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->longText('title');
            $table->longText('subtitle')->nullable();
            $table->longText('short_text')->nullable();
            $table->longText('long_text')->nullable();
            SeoFacade::addDatabaseFields($table);
            $table->unsignedInteger('order_column')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('include_team')->default(false);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        
        // No llamamos a los seeders en los test porque los ralentizan
        // y no es necesario
        if (!App::runningUnitTests()) {
            Artisan::call('page:install');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
