<?php

declare(strict_types=1);

namespace Bittacora\Page\Database\Seeders;

use Bittacora\Page\Database\Seeders\Seeds\PageContentMultimediaLocationSeeder;
use Bittacora\Tabs\Tabs;
use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 * @package Bittacora\Page\Database\Seeders
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            PageContentMultimediaLocationSeeder::class
        ]);

        Tabs::createItem('page.index', 'page.index', 'page.index', 'Páginas de contenido','fa fa-list');
        Tabs::createItem('page.index', 'page.create', 'page.create', 'Nuevo','fa fa-plus');

        Tabs::createItem('page.create', 'page.index', 'page.index', 'Páginas de contenido','fa fa-list');
        Tabs::createItem('page.create', 'page.create', 'page.create', 'Nuevo','fa fa-plus');

        Tabs::createItem('page.edit', 'page.index', 'page.index', 'Páginas de contenido','fa fa-list');
        Tabs::createItem('page.edit', 'page.create', 'page.create', 'Nuevo','fa fa-plus');

    }
}
